package com.allianz.test.application;

import com.allianz.test.domain.WeatherService;
import com.allianz.test.infrastructure.client.dto.GetWeatherResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class GetWeatherByCityNameTest {

    @InjectMocks
    GetWeatherByCityName getWeatherByCityName;

    @Mock
    WeatherService weatherService;

    @Test
    void should_return_weather_when_provide_valid_city_name() throws Exception {

        given(weatherService.getWeatherByCity(anyString()))
                .willReturn(GetWeatherResponse.builder().build());

        GetWeatherResponse apply = getWeatherByCityName.apply(anyString());

        verify(weatherService).getWeatherByCity(anyString());
        assertThat(apply)
                .isNotNull()
                .hasAllNullFieldsOrProperties();
    }

}