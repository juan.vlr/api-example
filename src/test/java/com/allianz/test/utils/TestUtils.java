package com.allianz.test.utils;

import feign.FeignException;
import feign.Request;

import java.nio.charset.StandardCharsets;
import java.util.Collections;

public class TestUtils {


    public static final FeignException.NotFound NOT_FOUND_EXCEPTION_MOCK = new FeignException.NotFound(
            "", Request.create(
            Request.HttpMethod.GET,
            "http://local.mock/weather",
            Collections.emptyMap(),
            new byte[1],
            StandardCharsets.UTF_8
    ), new byte[1]
    );

    public static final FeignException.Unauthorized UNAUTHORIZED_EXCEPTION_MOCK = new FeignException.Unauthorized(
            "", Request.create(
            Request.HttpMethod.GET,
            "http://local.mock/weather",
            Collections.emptyMap(),
            new byte[1],
            StandardCharsets.UTF_8
    ), new byte[1]
    );


    public static final String JSON_WEATHER_RESPONSE = """
            {"coord":{"lon":-3.7026,"lat":40.4165},"weather":[{"id":801,"main":"Clouds","description":"few clouds",
            "icon":"02n"}],"base":"stations","main":{"temp":286.38,"feels_like":285.77,"temp_min":285.93,"temp_max":287.15,
            "pressure":1016,"humidity":77},"visibility":10000,"wind":{"speed":1.54,"deg":10},
            "clouds":{"all":20},"dt":1619120491,"sys":{"type":1,"id":6443,"country":"ES"
            ,"sunrise":1619069152,"sunset":1619118021},"timezone":7200,"id":3117735,"name":"Madrid","cod":200}
            """;

}
