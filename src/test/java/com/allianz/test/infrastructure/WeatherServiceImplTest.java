package com.allianz.test.infrastructure;

import com.allianz.test.infrastructure.client.OpenWeatherClient;
import com.allianz.test.infrastructure.client.dto.GetWeatherResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static com.allianz.test.utils.TestUtils.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;

@ExtendWith(MockitoExtension.class)
class WeatherServiceImplTest {

    @InjectMocks
    WeatherServiceImpl weatherService;

    @Mock
    private OpenWeatherClient openWeatherClient;

    @Test
    void should_get_weather_when_provide_valid_city_name() throws Exception {

        assertNotNull(weatherService);
        ReflectionTestUtils.setField(weatherService,"apiKey", "key");

        given(openWeatherClient.getWeatherByCityName("city", "key"))
                .willReturn(new ObjectMapper().readValue(JSON_WEATHER_RESPONSE, GetWeatherResponse.class));

        GetWeatherResponse weatherByCity = weatherService.getWeatherByCity("city");

        assertThat(weatherByCity)
            .isNotNull()
            .hasNoNullFieldsOrProperties();
    }

    @Test
    void should_get_exception_when_provide_invalid_city_name() throws Exception {

        assertNotNull(weatherService);
        ReflectionTestUtils.setField(weatherService,"apiKey", "key");

        doThrow(new RuntimeException("bad name"))
                .when(openWeatherClient).getWeatherByCityName(anyString(), anyString());

        final Throwable throwable = catchThrowable(() -> weatherService.getWeatherByCity("bad name"));

        assertThat(throwable)
                .isInstanceOf(Exception.class)
                .hasMessageContaining("bad");
    }

    @Test
    void should_get_weather_exception_when_provide_invalid_city_name() throws Exception {

        assertNotNull(weatherService);
        ReflectionTestUtils.setField(weatherService,"apiKey", "key");

        doThrow(NOT_FOUND_EXCEPTION_MOCK)
                .when(openWeatherClient).getWeatherByCityName(anyString(), anyString());

        final Throwable throwable = catchThrowable(() -> weatherService.getWeatherByCity("bad name"));

        assertThat(throwable)
                .isInstanceOf(FeignException.NotFound.class)
                .hasFieldOrPropertyWithValue("status", 404);
    }

    @Test
    void should_get_unauthorized_exception_when_provide_invalid_city_name() throws Exception {

        assertNotNull(weatherService);
        ReflectionTestUtils.setField(weatherService,"apiKey", "key");

        doThrow(UNAUTHORIZED_EXCEPTION_MOCK)
                .when(openWeatherClient).getWeatherByCityName(anyString(), anyString());

        final Throwable throwable = catchThrowable(() -> weatherService.getWeatherByCity("bad name"));

        assertThat(throwable)
                .isInstanceOf(FeignException.Unauthorized.class)
                .hasFieldOrPropertyWithValue("status", 401);
    }




}