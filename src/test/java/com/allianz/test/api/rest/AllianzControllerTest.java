package com.allianz.test.api.rest;

import com.allianz.test.application.GetWeatherByCityName;
import com.allianz.test.domain.WeatherService;
import com.allianz.test.infrastructure.client.dto.Clouds;
import com.allianz.test.infrastructure.client.dto.GetWeatherResponse;
import com.allianz.test.infrastructure.client.dto.Main;
import com.allianz.test.infrastructure.client.dto.Weather;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class AllianzControllerTest {

    private static final String CITY_NAME = "Madrid";

    private final WeatherService weather = mock(WeatherService.class);
    private final GetWeatherByCityName getWeatherByCityName = new GetWeatherByCityName(weather);

    @Test
    void should_return_ok_response_when_given_valid_cityname() throws Exception {

        // GIVEN
        final GetWeatherResponse weatherResponse = buildResponse();
        when(weather.getWeatherByCity(anyString())).thenReturn(weatherResponse);

        // WHEN
        GetWeatherResponse response = getWeatherByCityName.apply(CITY_NAME);

        // THEN
        assertThat(response)
                .isNotNull()
                .hasAllNullFieldsOrPropertiesExcept("main", "clouds", "weather");
    }

    private GetWeatherResponse buildResponse() {
        return GetWeatherResponse.builder()
                .clouds(new Clouds(1))
                .main(Main.builder().temp(1D).build())
                .weather(List.of(Weather.builder().description("good").build()))
                .build();
    }

}