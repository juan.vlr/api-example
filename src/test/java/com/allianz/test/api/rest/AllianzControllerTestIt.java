package com.allianz.test.api.rest;

import com.allianz.test.api.rest.dto.ConditionDTO;
import com.allianz.test.api.rest.dto.TemperatureDTO;
import com.allianz.test.api.rest.dto.WeatherDTO;
import com.allianz.test.api.rest.dto.WindSpeedDTO;
import com.allianz.test.infrastructure.client.OpenWeatherClient;
import com.allianz.test.infrastructure.client.dto.GetWeatherResponse;
import com.allianz.test.infrastructure.client.dto.Main;
import com.allianz.test.infrastructure.client.dto.Weather;
import com.allianz.test.infrastructure.client.dto.Wind;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static com.allianz.test.utils.TestUtils.NOT_FOUND_EXCEPTION_MOCK;
import static com.allianz.test.utils.TestUtils.UNAUTHORIZED_EXCEPTION_MOCK;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AllianzControllerTestIt {

    private static final String CITY_NAME = "Madrid";
    private static final String CITY_INVALID = "Madrostoles";
    private static final String GET_WEATHER_BY_CITY = "/weather/{city}";
    private static final String WEATHER_GIVEN = "good";
    private static final String WEATHER_EXPECTED = "good";

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OpenWeatherClient weatherService;

    @Test
    void should_returnOKResponse_when_given_validCityName() throws Exception {

        // GIVEN
        when(weatherService.getWeatherByCityName(eq(CITY_NAME), any())).thenReturn(buildResponse());

        // WHEN
        final ResultActions resultActions = mockMvc.perform(
                get(GET_WEATHER_BY_CITY, CITY_NAME)
                        .contentType(MediaType.APPLICATION_JSON));

        // THEN
        resultActions
                .andExpect(status().isOk())
                .andExpect(content().json(jsonToString(mockResponseDTO())));
    }

    @Test
    void should_return_notFound_Response_when_given_invalidCityName() throws Exception {

        // GIVEN
        doThrow(new RuntimeException())
                .when(weatherService).getWeatherByCityName(anyString(), anyString());

        // WHEN
        final ResultActions resultActions = mockMvc.perform(
                get(GET_WEATHER_BY_CITY, CITY_INVALID)
                        .contentType(MediaType.APPLICATION_JSON));

        // THEN
        resultActions
                .andExpect(status().isInternalServerError());
    }

    @Test
    void should_return_not_found_response_when_given_invalid_city_name() throws Exception {

        // GIVEN
        doThrow(NOT_FOUND_EXCEPTION_MOCK)
                .when(weatherService).getWeatherByCityName(anyString(), anyString());

        // WHEN
        final ResultActions resultActions = mockMvc.perform(
                get(GET_WEATHER_BY_CITY, CITY_INVALID)
                        .contentType(MediaType.APPLICATION_JSON));

        // THEN
        resultActions
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_unauthorized_response_when_given_invalid_city_name() throws Exception {

        // GIVEN
        doThrow(UNAUTHORIZED_EXCEPTION_MOCK)
                .when(weatherService).getWeatherByCityName(anyString(), anyString());

        // WHEN
        final ResultActions resultActions = mockMvc.perform(
                get(GET_WEATHER_BY_CITY, CITY_INVALID)
                        .contentType(MediaType.APPLICATION_JSON));

        // THEN
        resultActions
                .andExpect(status().isUnauthorized());
    }

    private GetWeatherResponse buildResponse() {
        return GetWeatherResponse.builder()
                .wind(new Wind(1D, 1))
                .main(Main.builder().temp(1D).build())
                .weather(List.of(Weather.builder().description(WEATHER_GIVEN).build()))
                .build();
    }

    private static <T> String jsonToString(T object) throws JsonProcessingException {
        return OBJECT_MAPPER.writeValueAsString(object);
    }

    public static WeatherDTO mockResponseDTO() {
        return WeatherDTO.builder()
                .condition(ConditionDTO.builder().value(WEATHER_EXPECTED).build())
                .temperature(TemperatureDTO.builder().value(1D).build())
                .windSpeed(WindSpeedDTO.builder().value(1D).build())
                .build();
    }

}