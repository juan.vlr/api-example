package com.allianz.test.application;

import com.allianz.test.domain.WeatherService;
import com.allianz.test.infrastructure.client.dto.GetWeatherResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.function.Function;

@Component
@RequiredArgsConstructor
public class GetWeatherByCityName implements Function<String, GetWeatherResponse> {

    private final WeatherService weatherService;

    @SneakyThrows
    @Override
    public GetWeatherResponse apply(String city) {

        return weatherService.getWeatherByCity(city);
    }
}
