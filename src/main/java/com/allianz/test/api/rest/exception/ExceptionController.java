package com.allianz.test.api.rest.exception;

import com.allianz.test.api.rest.dto.response.GenericResponseErrorDTO;
import com.allianz.test.domain.exception.WeatherException;
import feign.FeignException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {FeignException.NotFound.class})
    protected ResponseEntity<Object> handleApplicationException(final FeignException.NotFound ex,
                                                                final WebRequest request) {
        String message = "Weather not found for city : %s".formatted(getCityName(request));
        return handleExceptionInternal(ex, composeExceptionBody(message, HttpStatus.NOT_FOUND),
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = {FeignException.Unauthorized.class})
    protected ResponseEntity<Object> handleApplicationException(final FeignException.Unauthorized ex,
                                                                final WebRequest request) {
        String message = "Invalid API key";
        return handleExceptionInternal(ex, composeExceptionBody(message, HttpStatus.UNAUTHORIZED),
                new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler(value = {WeatherException.class, FeignException.class})
    protected ResponseEntity<Object> handleApplicationException(final Exception ex,
                                                                final WebRequest request) {
        String message = "Unexpected exception has been reported during your request";
        return handleExceptionInternal(ex, composeExceptionBody(message, HttpStatus.INTERNAL_SERVER_ERROR),
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    private Object composeExceptionBody(String message, HttpStatus httpStatus) {
        return GenericResponseErrorDTO.builder()
                .code(httpStatus.value())
                .message(message)
                .build();
    }

    private String getCityName(WebRequest request) {
        String[] items = ((ServletWebRequest) request).getRequest().getRequestURL().toString().split("/");
        return items.length > 0 ? items[items.length-1] : "";
    }

}
