package com.allianz.test.api.rest;

import com.allianz.test.api.rest.dto.WeatherDTO;
import com.allianz.test.api.rest.dto.response.GenericResponseErrorDTO;
import com.allianz.test.api.rest.mapper.WeatherMapper;
import com.allianz.test.application.GetWeatherByCityName;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/weather")
public class AllianzController {

    private final GetWeatherByCityName weather;
    private final WeatherMapper mapper;

    @Operation(
            summary = "Obtain the weather",
            description = "Obtain the weather given the city name")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the city",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = WeatherDTO.class))}),
            @ApiResponse(responseCode = "500", description = "Unexpected problem has been found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GenericResponseErrorDTO.class))}),
            @ApiResponse(responseCode = "404", description = "City not found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GenericResponseErrorDTO.class))}),
            @ApiResponse(responseCode = "401", description = "ApiKey not valid",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GenericResponseErrorDTO.class))})})
    @GetMapping("/{city}")
    public WeatherDTO getWeather(
            @Parameter(description = "City name", required = true, name = "city")
            @PathVariable("city") String city) {

        return mapper.toWeatherDTO(weather.apply(city));
    }

}