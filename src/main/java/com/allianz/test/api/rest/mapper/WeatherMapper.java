package com.allianz.test.api.rest.mapper;


import com.allianz.test.api.rest.dto.*;
import com.allianz.test.infrastructure.client.dto.GetWeatherResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface WeatherMapper {

    @Mapping(source = "main.temp", target = "temperature.value")
    @Mapping(source = "wind.speed", target = "windSpeed.value")
    @Mapping(target = "condition.value", expression = "java(getWeatherResponse.getWeather().get(0).getDescription())")
    WeatherDTO toWeatherDTO(GetWeatherResponse response);
}
