package com.allianz.test.domain.exception;

public class WeatherException extends RuntimeException {

  public WeatherException(String message, Throwable throwable) {
    super(message, throwable);
  }

}
