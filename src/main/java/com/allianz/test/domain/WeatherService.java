package com.allianz.test.domain;

import com.allianz.test.infrastructure.client.dto.GetWeatherResponse;

public interface WeatherService {

    GetWeatherResponse getWeatherByCity(String city) throws Exception;
}
