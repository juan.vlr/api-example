package com.allianz.test.infrastructure;

import com.allianz.test.domain.WeatherService;
import com.allianz.test.domain.exception.WeatherException;
import com.allianz.test.infrastructure.client.OpenWeatherClient;
import com.allianz.test.infrastructure.client.dto.GetWeatherResponse;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class WeatherServiceImpl implements WeatherService {

    @Value("${service.open-weather.api-key}")
    private String apiKey;

    private final OpenWeatherClient openWeatherClient;

    @Cacheable(value = "weather")
    public GetWeatherResponse getWeatherByCity(String city) throws Exception {

        try {
            return openWeatherClient.getWeatherByCityName(city, apiKey);
        } catch (final FeignException.NotFound exception) {
            throw exception;
        } catch (final FeignException.Unauthorized exception) {
            throw exception;
        } catch (final FeignException exception) {
            throw exception;
        } catch (final Exception exception) {
            throw new WeatherException(city, exception);
        }
    }
}
