package com.allianz.test.infrastructure.client.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data

@Builder
@AllArgsConstructor
public class Weather implements Serializable {

    private Integer id;
    private String main;
    private String description;
    private String icon;
}
