package com.allianz.test.infrastructure.client.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Wind implements Serializable {

    private Double speed;
    private Integer deg;
}