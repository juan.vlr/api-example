package com.allianz.test.infrastructure.client;


import com.allianz.test.infrastructure.client.dto.GetWeatherResponse;

public interface OpenWeatherClient {

    GetWeatherResponse getWeatherByCityName(String city, String apiKey);
}
