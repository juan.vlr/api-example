package com.allianz.test.infrastructure.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetWeatherResponse implements Serializable {

  @JsonProperty("coord")
  private Coord coord;
  @JsonProperty("base")
  private String base;
  @JsonProperty("weather")
  private List<Weather> weather;
  @JsonProperty("main")
  private Main main;
  @JsonProperty("visibility")
  private Integer visibility;
  @JsonProperty("wind")
  private Wind wind;
  @JsonProperty("clouds")
  private Clouds clouds;
  @JsonProperty("dt")
  private Integer dt;
  @JsonProperty("sys")
  private Sys sys;
  @JsonProperty("timezone")
  private Integer timeZone;
  @JsonProperty("id")
  private Integer id;
  @JsonProperty("name")
  private String name;
  @JsonProperty("cod")
  private Integer cod;
}
