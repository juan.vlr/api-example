package com.allianz.test.infrastructure.client.dto;

import lombok.AllArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
public class Coord implements Serializable {

    private Double lon;
    private Double lat;
}
