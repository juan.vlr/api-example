package com.allianz.test.infrastructure.client;

import com.allianz.test.infrastructure.client.dto.GetWeatherResponse;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(
    name = "open-weather",
    url = "${service.open-weather.url}",
    fallback = OpenWeatherClientFallback.class
)
public interface OpenWeatherFeignClient extends OpenWeatherClient {

    @RequestLine("GET /weather?q={city}&appid={apiKey}")
    GetWeatherResponse getWeatherByCityName(@Param("city") String city, @Param("apiKey") String apiKey);
}
