package com.allianz.test.infrastructure.client.dto;

import lombok.AllArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
public class Clouds implements Serializable {

    private Integer all;
}
