package com.allianz.test.infrastructure.client;

import com.allianz.test.domain.exception.WeatherException;
import com.allianz.test.infrastructure.client.dto.GetWeatherResponse;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OpenWeatherClientFallback implements OpenWeatherFeignClient {

  private final Exception cause;

  @Override
  public GetWeatherResponse getWeatherByCityName(String city, String apiKey) {
    throw new WeatherException(city, cause);
  }
}
