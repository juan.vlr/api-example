package com.allianz.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class AllianzTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AllianzTestApplication.class, args);
	}
}
